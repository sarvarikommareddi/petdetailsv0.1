package handler

import (
	"Http-Handlers/dev/models"
	"bytes"
	"encoding/json"

	"net/http"
)

func (s *Server) SavePetDetails(w http.ResponseWriter, r *http.Request) {
	var petData models.Pet

	if err := json.NewDecoder(r.Body).Decode(&petData); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))

		return
	}

	if petData.ID == 0 || petData.Price == 0 || petData.Type == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid pet details, required valid id, proce and type"))

		return
	}

	jsonD, _ := json.Marshal(petData)

	req, err := http.NewRequest(http.MethodPost, "http://petstore-demo-endpoint.execute-api.com/petstore/pets", bytes.NewReader(jsonD))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))

		return
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := s.Client.Do(req)
	if err != nil {
		w.WriteHeader(http.StatusBadGateway)
		w.Write([]byte(err.Error()))

		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("failed to save pet details in db"))

		return
	}

	//write response
	w.WriteHeader(http.StatusOK)

}

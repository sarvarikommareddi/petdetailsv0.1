package handler

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Server struct {
	Client *http.Client
}

func NewServer() *Server {
	return &Server{
		Client: http.DefaultClient,
	}
}

func (s *Server) Start() {
	r := mux.NewRouter()

	r.HandleFunc("/api/pets", s.SavePetDetails).Methods(http.MethodPost)
	r.HandleFunc("/api/pets", s.ListPets).Methods(http.MethodGet)
	r.HandleFunc("/api/pet/{id}", s.GetPetByID).Methods(http.MethodGet)

	log.Println("started server on port: 8080")

	http.ListenAndServe(":8080", r)
}

package main

import "Http-Handlers/dev/api/handler"

func main() {
	server := handler.NewServer()

	server.Start()
}
